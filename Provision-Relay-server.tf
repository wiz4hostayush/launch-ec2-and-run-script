provider "aws"{
  profile = var.profile
  region  = var.region
}

resource "aws_security_group" "server-sg" {
  name        = "Relay-server-sg"
  description = "SG For Relay Sever"
  vpc_id      = var.vpc_id
  ingress {
    description = "All traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_instance" "relay-server-ec2" {
  ami             = var.amiId
  instance_type   = var.instance_type
  subnet_id       = var.subnet_id
  vpc_security_group_ids = [aws_security_group.server-sg.id]
  key_name        = var.key_name
  user_data = file("IIS_script.txt")
  tags = {
    Name = "RelayServer"
  }
}

