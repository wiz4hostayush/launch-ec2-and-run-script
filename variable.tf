variable region {
  type        = string
  default     = "ap-south-1"
  description = "region"
}
variable profile {
  type        = string
  default     = "default"
  description = "profile name for the aws using in aws cli"
}

variable keypath {
  type        = string
  default     = ""
  description = "Path to pem key file required for EC2 Instance"
}

variable amiId {
  type        = string
  default     = "ami-01cb77abecc7c4e46"
  description = "AMI ID for EC2 Instance above used AMI Microsoft Windows Server 2016 Base"
}

variable instance_type {
  type        = string
  default     = "t2.micro"
  description = "Instance Type for EC2"
}

variable key_name {
  type        = string
  default     = ""
  description = "Keypair Name for EC2"
}

variable vpc_id {
  type        = string
  default     = ""
  description = "VPC ID for security group"
}

variable subnet_id {
  type       = string
  default     = ""
  description = "Subnet ID for EC2"
}
